/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.binaryrepositorymanager.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* Credential
*/
@ApiModel(description = "Credential")
public class Credential {

    protected String username = null;
    protected String password = null;


    /**
    * Quick Set username
    * Useful setter to use in builder style (eg. myCredential.username(String).anotherQuickSetter(..))
    * @param String username
    * @return Credential The modified Credential object
    **/
    public Credential username(String username) {
        this.username = username;
        return this;
    }

    /**
    * username
    * @return String username
    **/
    @ApiModelProperty(value = "username")
    public String getUsername() {
        return username;
    }

    /**
    * username
    **/
    public void setUsername(String username) {
        this.username = username;
    }

    /**
    * Quick Set password
    * Useful setter to use in builder style (eg. myCredential.password(String).anotherQuickSetter(..))
    * @param String password
    * @return Credential The modified Credential object
    **/
    public Credential password(String password) {
        this.password = password;
        return this;
    }

    /**
    * password
    * @return String password
    **/
    @ApiModelProperty(value = "password")
    public String getPassword() {
        return password;
    }

    /**
    * password
    **/
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Credential credential = (Credential) o;
        return Objects.equals(this.username, credential.username) &&
        Objects.equals(this.password, credential.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }
}

