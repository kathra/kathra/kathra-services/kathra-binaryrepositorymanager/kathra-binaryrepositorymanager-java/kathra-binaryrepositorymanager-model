
# Credential

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** | username |  [optional]
**password** | **String** | password |  [optional]



