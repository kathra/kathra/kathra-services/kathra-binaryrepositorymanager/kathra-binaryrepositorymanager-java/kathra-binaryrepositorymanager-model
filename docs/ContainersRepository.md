
# ContainersRepository

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | name |  [optional]
**metadata** | [**ContainersRepositoryMetadata**](ContainersRepositoryMetadata.md) |  |  [optional]



