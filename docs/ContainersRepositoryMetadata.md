
# ContainersRepositoryMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isPublic** | **String** |  |  [optional]
**scanImagesOnPush** | **String** |  |  [optional]
**contentTrustEnabled** | **String** |  |  [optional]
**preventVulnerableFromRunning** | **String** |  |  [optional]
**vulnerableThreshold** | [**VulnerableThresholdEnum**](#VulnerableThresholdEnum) |  |  [optional]


<a name="VulnerableThresholdEnum"></a>
## Enum: VulnerableThresholdEnum
Name | Value
---- | -----
LOW | &quot;low&quot;
MEDIUM | &quot;medium&quot;
HIGH | &quot;high&quot;



